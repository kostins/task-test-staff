{$obj_prefix = "`$block.block_id`000"}

<script type ="text/javascript">
    function fn_mail_update(item_id)
    {
        $.ceAjax('request', fn_url('staff.get_email&item_id='+item_id), {
            method: 'POST',
            callback: function(data) {
            	var mail = data.text;
            	var t_mail = mail.slice(0,18);
                $('#staff_email_div_' + item_id).html("<a  href=\"mailto:\"" + data.text + "\">" + t_mail + "</a>");
	        }
        });
    }
</script>

<div class="owl-theme ty-owl-controls">
    <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
        <div class="owl-buttons">
            <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
            <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
        </div>
    </div>
</div>


<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$staff item="sti" name="for_staff"}
        {foreach from=$sti item="st"}
		    <div class="ty-scroller-list__item">
		    <div class="ty-scroller-list__img-block">
	            {include file="common/image.tpl" images=$st.main_pair class="ty-banner__image" image_width="80" image_height="80"}<br>
	            <label>{$st.firstname|truncate:12}{$st.lastname|truncate:12}</label><br>
	            <label>{$st.function|truncate:12}</label><br>
	            {if $st.email}
	              <div id="staff_email_div_{$st.staff_id}"><a onclick="fn_mail_update({$st.staff_id});">{__("show_email")}</a></div>
              	{/if}
            </div>
	        </div>
        {/foreach}   
    {/foreach}
</div>

{include file="common/scroller_init.tpl" items=$staff prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
