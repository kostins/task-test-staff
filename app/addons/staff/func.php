<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/
 
 
 if (!defined('BOOTSTRAP')) { die('Access denied'); }

// GET

function fn_get_staff($params = array())
{
    $fields = array (
        '?:staff.staff_id',
        '?:staff.status',
        '?:staff.firstname',
        '?:staff.lastname',
        '?:staff.function',
        '?:staff.email',
        '?:staff.user_id',
        '?:staff_images.staff_image_id'
    );
    /*
    ,
        '?:staff_images.staff_image_id',
        */
    $condition = '';   
    
    $joins[] = db_quote("LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id");

    if (!empty($params['staff_id'])) {
        $condition .= db_quote(' AND ?:staff.staff_id = ?i', $params['staff_id']);
    }
       
    if (!empty($params['firstname'])) {
        $condition .= db_quote(' AND ?:staff.firstname = ?s', $params['firstname']);
    }

    if (!empty($params['lastname'])) {
        $condition .= db_quote(' AND ?:staff.lastname = ?s', $params['lastname']);
    }
    
    if (!empty($params['function'])) {
        $condition .= db_quote(' AND ?:staff.function = ?s', $params['function']);
    }

    if (!empty($params['email'])) {
        $condition .= db_quote(' AND ?:staff.email = ?s', $params['email']);
    }

    if (!empty($params['user_id'])) {
        $condition .= db_quote(' AND ?:staff.user_id = ?s', $params['user_id']);
    }

    if (!empty($params['status'])) {
        $condition .= db_quote(' AND ?:staff.status = ?s', $params['status']);
    }

    $sortings = array(
        'firstname' => '?:staff.firstname',
        'function' => '?:staff.function',
    );

    $sorting = db_sort($params, $sortings, 'firstname', 'asc');

    $staff = db_get_array("SELECT ?p FROM ?:staff ?p WHERE 1 ?p ?p", implode(", ", $fields), implode(" ", $joins), $condition, $sorting);
    
    foreach($staff as $i => $st){
        if (!empty($st)) {
            $staff[$i]['main_pair'] = fn_get_image_pairs($staff[$i]['staff_image_id'], 'staff', 'M', true, false);
        }            
    }

    return array($staff);
}

//  one GET

function fn_get_staff_data($staff_id)
{
    $fields = array (
        '?:staff.staff_id',
        '?:staff.status',
        '?:staff.firstname',
        '?:staff.lastname',
        '?:staff.function',
        '?:staff.email',
        '?:staff.user_id',
        '?:staff_images.staff_image_id',
    );
    
    $condition='';
    $joins[] = db_quote("LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id");

    if (!empty($staff_id)) {
      $condition .= db_quote(' AND ?:staff.staff_id = ?i', $staff_id);
    }

    $staff = db_get_row("SELECT ?p FROM ?:staff ?p WHERE 1 ?p", implode(", ", $fields), implode(" ", $joins), $condition);

    if (!empty($staff)) {
        $staff['main_pair'] = fn_get_image_pairs($staff['staff_image_id'], 'staff', 'M', true, false);
    }
   
    if (!empty($staff['user_id'])) {
        
        if (empty($staff['firstname'])) {
            $staff['firstname'] = db_get_field("SELECT firstname FROM ?:users WHERE user_id = ?i", $staff['user_id']);
        }

        if (empty($staff['lastname'])) {
            $staff['lastname'] = db_get_field("SELECT lastname FROM ?:users WHERE user_id = ?i", $staff['user_id']);
        }

        if (empty($staff['email'])) {
            $staff['email'] = db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $staff['user_id']);
        }
    }

    return $staff;
}

// update

function fn_staff_update_staff($data, $staff_id)
{
    if (!empty($staff_id)) {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $staff_id);

    } else {
        $staff_id = $data['staff_id'] = db_query("REPLACE INTO ?:staff ?e", $data);
    }

        $staff_image_id = db_get_field("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
   
        $image_is_update = fn_staff_need_image_update();

        if (fn_staff_need_image_update()) {
            $staff_image_id = db_get_next_auto_increment_id('staff_images');
            $pair_data = fn_attach_image_pairs('staff_main', 'staff', $staff_image_id);
            if (!empty($pair_data)) { 
                $data_staff_image = array(
                    'staff_image_id' => $staff_image_id,
                    'staff_id'       => $staff_id
                );

                db_query("INSERT INTO ?:staff_images ?e", $data_staff_image);
            }
        }

    return $staff_id;
}

function fn_delete_staff_by_id($staff_id)
{
    if (!empty($staff_id)) {
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
    }
}

function fn_staff_need_image_update()
{
    if (!empty($_REQUEST['file_staff_main_image_icon']) && array($_REQUEST['file_staff_main_image_icon'])) {
        $image_staff = reset ($_REQUEST['file_staff_main_image_icon']);

        if ($image_staff == 'staff_main') {
            return false;
        }
    }

    return true;
}
