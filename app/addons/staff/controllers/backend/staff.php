<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {


    if ($mode == 'update') {
        $staff_id = fn_staff_update_staff($_REQUEST['staff'], $_REQUEST['staff_id']);

        $suffix = ".update?staff_id=$staff_id";
    }
    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_delete_staff_by_id($_REQUEST['staff_id']);
        }

        $suffix = '.manage';
    }

return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);

}

//	UPDATE

if ($mode == 'update') {
    $staff = fn_get_staff_data($_REQUEST['staff_id']);    

    if (empty($staff)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
    
    Tygh::$app['view']->assign('staff', $staff);

} 

//	MANAGE

elseif ($mode == 'manage') {
    list($staff) = fn_get_staff(array());
    Tygh::$app['view']->assign('staff', $staff);

}