<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD']  == 'POST') {
	if ($mode == 'get_email') {
		
		if ($_REQUEST['item_id']) {

			$staff_id = $_REQUEST['item_id'];

			if (!empty($staff_id)) {
			    $data = db_get_field("SELECT email FROM ?:staff WHERE staff_id = ?i", $staff_id);
			}
		}
		
	    //Registry::get('view')->display('addons/staff/blocks/staff.tpl');

	    echo($data);
	    exit;
	}
}


