{capture name="mainbox"}


<form action="{""|fn_url}" method="post" name="pages_tree_form" id="pages_tree_form">

{if $staff}
<table class="table table-middle">
<thead>
<tr>
    <th width="20%">{__("name")}</th>
    <th width="30%">{__("func_user")}</th>
    <th width="40%">{__("email")}</th>
    <th width="35%">{__("user")}</th>
    <th width="5%">&nbsp;</th>
    <th width="10%" class="right"><a>{__("status")}</a></th>
</tr>
</thead>
{foreach from=$staff item=st}
    <td class="nowrap row-status cm-no-hide-input">{$st.firstname|truncate:15} {$st.lastname|truncate:15}</td>
    <td class="nowrap row-status cm-no-hide-input">{$st.function|truncate:50}</td>
    <td class="nowrap row-status cm-no-hide-input">{$st.email|truncate:20}</td>
    <td class="nowrap row-status cm-no-hide-input">{if $st.user_id}<a href="admin.php?dispatch=profiles.update&user_id="+$user_id+"">{__("edit_user")}</a>{else}<p></p>{/if}</td>
    <td class="center" width="10%">
            <div class="hidden-tools">
              <div class="btn-group dropleft">
                <a class="btn dropdown-toggle" data-toggle="dropdown">
                   <i class="icon-cog"></i>
                      <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                        <li>
                            <a href="{"staff.update&staff_id=`$st.staff_id`"|fn_url}">{__("edit")}</a>
                        </li>
                        <li>
                            <a href="{"staff.delete&staff_id=`$st.staff_id`"|fn_url}" class="cm-confirm cm-post" data-ca-confirm-text="This will delete all selected staff.">{__("delete")}</a>
                        </li>
                </ul>
             </div>
           </div>
       </td>
   <td class="right nowrap">
        {include file="common/select_popup.tpl" popup_additional_class="dropleft" id=$st.staff_id status=$st.status hidden=true object_id_name="staff_id" table="staff"}
   </td>
       
 </tr>
{/foreach}

</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
</form>

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("staffadd") icon="icon-plus"}
{/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons}

