{if $staff}
    {assign var="staff_id" value=$staff.staff_id}
{else}
    {assign var="staff_id" value=0}
{/if}


{assign var="allow_save" value=$staff|fn_allow_save_object:"staff"}

{assign var="b_type" value=$staff.type|default:"G"}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  {if !$allow_save} cm-hide-inputs{/if}" name="staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="staff_id" value="{$staff.staff_id}" />

{capture name="tabsbox"}
    <div id="content_general">
      <div class="control-group">
        <label for="elm_staff_firstname" class="control-label">{__("first_name")}</label>
        <div class="controls">
        <input type="text" name="staff[firstname]" id="elm_staff_firstname" value="{$staff.firstname}" size="25" class="input-large" />
        </div>
      </div>
      <div class="control-group">
        <label for="elm_staff_lastname" class="control-label">{__("last_name")}</label>
        <div class="controls">
          <input type="text" name="staff[lastname]" id="elm_staff_lastname" value="{$staff.lastname}" size="25" class="input-large" />
        </div>
      </div> 
      <div class="control-group">
        <label for="elm_staff_email" class="control-label">{__("email")}</label>
        <div class="controls">
          <input type="text" name="staff[email]" id="elm_staff_email" value="{$staff.email}" size="25" class="input-large" />
        </div>
      </div>
      <div class="control-group">
        <label for="elm_staff_function" class="control-label cm-required">{__("func_user")}</label>
        <div class="controls">
          <textarea id="elm_staff_function" name="staff[function]" class="formMessage">{$staff.function}
          </textarea>
        </div>
      </div>
      <div class="control-group">
        <label for="elm_staff_user" class="control-label">{__("user_id")}</label>
        <div class="controls">
          <input type="text" name="staff[user_id]" id="elm_staff_user" value="{$staff.user_id}" size="25" class="input-large" />
        </div>
      </div>
    </div>
      {include file="common/select_status.tpl" input_name="staff[status]" id="elm_st_status" obj_id=$staff_id obj=$staff hidden=true}
      <div class="control-group {if $b_type != "G"}hidden{/if}" id="staff_graphic">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_main" image_object_type="staff" image_pair=$staff.main_pair image_object_id=$staff.staff_id no_detailed=true hide_titles=true}
        </div>
      </div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$staff_id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
    {else}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$staff_id}
    {/if}
{/capture}
    
</form>

{/capture}

{notes}

{/notes}

{if !$staff_id}
    {assign var="title" value=__("staffnew")}
{else}
    {assign var="title" value="{__("staff")}: `$staff.function`"}
{/if}
{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}

